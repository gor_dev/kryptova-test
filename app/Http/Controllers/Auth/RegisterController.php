<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Services\UserService;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show registration form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $allRoles = User::getRoles();

        return view('auth.register', compact('allRoles'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(RegisterRequest $request, UserService $userService)
    {
        $password = Hash::make($request->get('password'));

        $date = Carbon::parse($request->get('dob'));
        $now = Carbon::now();
        $age = $date->diffInYears($now);
        $data = array_merge($request->validated(), [
            'password' => $password,
            'age' => $age,
        ]);
        $user = User::create($data);

        $image = $userService->uploadImageAndGetName($request);
        $user->profile_picture = $image;
        $user->save();
        Auth::login($user);
        return redirect()->route('home')
            ->with(['success' => __('You successfully created an account in ') . config('app.name') . '.']);
    }
}
