<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;

    const IMAGES_FOLDER = 'images/users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'age',
        'dob',
        'address',
        'password',
        'role',
        'profile_picture',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get role text.
     *
     * @return array|mixed|null|string
     */
    public function getRoleTextAttribute()
    {
        return self::getRoles()[$this->role] ?? __('Unknown');
    }

    /**
     * Get available roles.
     *
     * @return array
     */
    public static function getRoles()
    {
        return [
            self::ROLE_ADMIN => __('Admin'),
            self::ROLE_MANAGER => __('Manager'),
        ];
    }
}
