@extends("layouts.main")

@section('title', __('Edit student') . ' ' . $student->full_name)

@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>{{ __('Edit student') . ' ' . $student->full_name }}</h4></div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('students.update', $student) }}">
                            @csrf
                            @method('put')
                            @include('students.partials.create-edit')
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                    <a href="{{ route('students.show', $student) }}">{{ __('Cancel') }}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
