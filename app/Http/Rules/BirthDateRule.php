<?php

namespace App\Http\Rules;

use App\Models\Common;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class BirthDateRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $date = Carbon::parse($value);

            $now = Carbon::now();
            $diffdays = $date->diffInDays($now);
            $diffYears = $date->diffInYears($now);

            return $diffdays > 0 && $diffYears < 150;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Date of birth is invalid.');
    }
}
