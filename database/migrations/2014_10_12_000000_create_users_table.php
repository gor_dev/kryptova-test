<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->id();
                $table->string('name')->index();
                $table->string('email')->unique();
                $table->tinyInteger('age')->nullable()->index();
                $table->date('dob')->nullable()->index();
                $table->string('address')->nullable()->index();
                $table->string('password');
                $table->tinyInteger('role')->index()->default(User::ROLE_MANAGER);
                $table->string('profile_picture')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
