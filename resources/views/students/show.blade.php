@extends("layouts.main")

@section('title', __('Show student') . ' ' . $student->full_name)

@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h4>{{ __('Show student') . ' ' . $student->full_name }}</h4></div>
                    <div class="card-body">
                        <p>{{ __('Firstname') . ': ' . $student->first_name }}</p>
                        <p>{{ __('Lastname') . ': ' . $student->last_name }}</p>
                        <p>{{ __('Address') . ': ' . $student->address }}</p>
                    </div>
                </div>
                <a href="{{ route('students.edit', $student) }}">{{ __('Edit') }}</a>
                <a href="{{ route('students.destroy', $student) }}"
                   onclick="event.preventDefault();
                           document.getElementById('delete-form-{{ $student->id }}').submit();">
                    {{ __('Delete') }}
                </a>
                <form id="delete-form-{{ $student->id }}" action="{{ route('students.destroy', $student) }}" method="POST" class="d-none">
                    @csrf
                    @method('delete')
                </form>
                <a href="{{ route('students.index') }}">{{ __('All Students') }}</a>
            </div>
        </div>
    </div>
@endsection
