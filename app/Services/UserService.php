<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Storage;
use Str;

class UserService
{
    /**
     * Upload user profile picture.
     *
     * @param Request $request
     * @return string
     */
    public function uploadImageAndGetName(Request $request)
    {
        if ($request->file('profile_picture')) {
            $file = $request->file('profile_picture');
            $ext = $file->getClientOriginalExtension();
            $nameSlug = Str::slug($request->get('name'));
            $image = $nameSlug . '-' . md5(now() . rand(1, 1000)) . '.' . $ext;
            Storage::disk('public')->put(User::IMAGES_FOLDER . '/' . $image, file_get_contents($file));
            return $image;
        }

        return '';
    }
}
