<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StudentsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', RegisterController::class . '@index')->name('register.show');
Route::post('/register', RegisterController::class . '@create')->name('register');

Route::get('/login', LoginController::class . '@index')->name('login.show');
Route::post('/login', LoginController::class . '@login')->name('login');
Route::post('/logout', LoginController::class . '@logout')->name('logout');

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/home', HomeController::class . '@index')->name('home');

    Route::resource('students', StudentsController::class);
});
