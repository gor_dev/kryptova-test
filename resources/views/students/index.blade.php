@extends("layouts.main")

@section('title', __('All students'))

@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <a href="{{ route('students.create') }}">{{ __('Create') }}</a>
                    <table>
                        <thead>
                        <tr>
                            <th>{{ __('Firtname') }}</th>
                            <th>{{ __('Lastname') }}</th>
                            <th>{{ __('Address') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($students as $student)
                            <tr>
                                <td>{{ $student->first_name }}</td>
                                <td>{{ $student->last_name }}</td>
                                <td>{{ $student->address }}</td>
                                <td>
                                    <a href="{{ route('students.show', $student) }}">{{ __('Show') }}</a>
                                    <a href="{{ route('students.edit', $student) }}">{{ __('Edit') }}</a>

                                    <a href="{{ route('students.destroy', $student) }}"
                                       onclick="event.preventDefault();
                                                document.getElementById('delete-form-{{ $student->id }}').submit();">
                                        {{ __('Delete') }}
                                    </a>
                                    <form id="delete-form-{{ $student->id }}" action="{{ route('students.destroy', $student) }}" method="POST" class="d-none">
                                        @csrf
                                        @method('delete')
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $students->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
