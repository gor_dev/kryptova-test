@extends("layouts.main")

@section('title', __('Home page'))

@section("content")
    @if(session()->has('success'))
        <span class="text-center d-block alert alert-success">
            <strong>{{ session()->get('success') }}</strong>
        </span>
        <br/> <br/>
    @endif
    <p class="text-center">{{ __('Welcome') . ' ' . $user->name }}</p>
    <img src="{{asset('/storage/' . App\Models\User::IMAGES_FOLDER . '/' . $user->profile_picture)}}"
         class="d-block col-md-2 m-auto" alt="{{ $user->name . ' ' . __('image') }}" />
    <p class="text-center">{{ __('You\'re ') . ' ' . $user->role_text }}</p>
    @php
        use \App\Models\User;
        if ($user->role == User::ROLE_ADMIN) {
            $welcomeText = __('You can do anything');
        } elseif ($user->role == User::ROLE_MANAGER) {
            $welcomeText = __('You can manage some things');
        }
    @endphp
    <p class="text-center">{{ $welcomeText }}</p>
@endsection
