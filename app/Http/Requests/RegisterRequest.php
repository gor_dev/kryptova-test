<?php

namespace App\Http\Requests;

use App\Http\Rules\BirthDateRule;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $allRolesStr = implode(',', array_keys(User::getRoles()));

        $maxFileSize = config('filesystems.images.user.max_file_size');
        $minWidth = config('filesystems.images.user.min_width');
        $minHeight = config('filesystems.images.user.min_height');
        $maxWidth = config('filesystems.images.user.max_width');
        $maxHeight = config('filesystems.images.user.max_height');

        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'max:32', 'confirmed'],
            'dob' => ['required', 'date_format:Y-m-d', new BirthDateRule()],
            'address' => ['required', 'string', 'max:255'],
            'role' => ['required', 'in:' . $allRolesStr],
            'profile_picture' => ['required', 'mimes:jpg,jpeg,png,', 'max:' . $maxFileSize, 'dimensions:min_width=' . $minWidth . ',min_height=' . $minHeight . ',max_width=' . $maxWidth . ',max_height=' . $maxHeight],
        ];
    }

    /**
     * Customize error messages for some rules
     *
     * @return array
     */
    public function messages()
    {
        $minWidth = config('filesystems.images.user.min_width');
        $minHeight = config('filesystems.images.user.min_height');
        $maxWidth = config('filesystems.images.user.max_width');
        $maxHeight = config('filesystems.images.user.max_height');

        return [
            'profile_picture.dimensions' => 'The Image has invalid dimensions, it should be: min - ' . $minWidth . 'px X ' . $minHeight . 'px, max - ' . $maxWidth . 'px X ' . $maxHeight . 'px.',
        ];
    }
}
