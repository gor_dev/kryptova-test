<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        $user = User::where([
            'email' => $request->get('email'),
        ])->first();

        if (Hash::check($request->get('password'), $user->password)) {
            if (!in_array($user->role, array_keys(User::getRoles()))) {
                throw ValidationException::withMessages(['email' => __('You don\'t have any accessable role to login.')]);
            }

            Auth::login($user);

            return $request->wantsJson()
                ? new JsonResponse([], 204)
                : redirect()->route('home');
        }
        throw ValidationException::withMessages(['email' => __('Invalid credentials.')]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }
}
