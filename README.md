# Test task for Gor from Kryptova #

The URL of the task is [this](https://www.evernote.com/shard/s125/sh/c9199f17-9822-ca13-ef51-74a56207c90a/a21372cf57078a2ba051e86eba62992a).

## Installation

1. Clone the repository to your working folder and cd into it
2. Run `composer install` to install dependencies. 
3. Add configuration data to `.env` file.
4. Run `php artisan migrate` to create necessary tables.
5. Run `php artisan key:generate` to generate an application encryption key.
6. Run `php artisan storage:link` to make a link to storage from public.
