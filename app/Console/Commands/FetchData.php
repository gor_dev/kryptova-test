<?php

namespace App\Console\Commands;

use App\Jobs\AddEmployee;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class FetchData extends Command
{
    const API_URL = 'https://dummy.restapiexample.com/api/v1/employees';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch employees data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $client = new Client();
            $request = $client->get(self::API_URL);
            $response = $request->getBody()->getContents();
            $content = json_decode($response, true);
            if (!empty($content['status']) && $content['status'] == 'success' && !empty($content['data'])) {
                foreach ($content['data'] as $employee) {
                    dispatch(new AddEmployee($employee));
                }
            }
        } catch (\Exception $e) {
            echo 'API error: ' . $e->getMessage();
        }
    }
}
