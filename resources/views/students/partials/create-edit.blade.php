<div class="form-group row">
    <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('Firstname') }}</label>
    <div class="col-md-6">
        <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror"
               name="first_name" value="{{ old('first_name', $student->first_name) }}" required autocomplete="first_name" autofocus>
        @error('first_name')
        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Lastname') }}</label>
    <div class="col-md-6">
        <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror"
               name="last_name" value="{{ old('last_name', $student->last_name) }}" autocomplete="last_name" autofocus>
        @error('last_name')
        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>
    <div class="col-md-6">
        <input type="text" id="address" class="form-control @error('address') is-invalid @enderror"
               name="address" value="{{ old('address', $student->address) }}" required autocomplete="address">
        @error('address')
        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
        @enderror
    </div>
</div>
